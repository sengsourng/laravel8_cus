# laravel8_cus


Web Development class at CUS-SR
```diff
+ This project student can learn detail about laravel 8.25
- Students can build their own projects
```
```diff
- text in red
+ text in green
! text in orange
# text in gray
+ text in purple (and bold)+
```

# Session 01 : How to create project laravel8_cus
យើង​ត្រូវប្រើប្រាស់ command ខាងក្រោម
```diff
$ composer create-project --prefer-dist laravel/laravel laravel8_cus 
```
ក្រោយមក RUN
```diff
$ php artisan serve
```
# Session 02 : How to Create User Login and Register
Go to your terminal type:
```diff
+ $ composer require laravel/ui
- $ php artisan ui vue --auth
! $ npm install && npm run dev	 

- if it can't install(npm install && npm run dev) you have to install Node.js software
```
# Session 03 : How to Create Models
Go to your terminal type:

	$ php artisan make:model Company -m

Go to file: Database/Migration/ Table: company

	public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
         $table->bigIncrements('id');
         $table->string('name');
         $table->string('address');
         $table->timestamps();
       });
     }
	
After you edit and save you have to run command:
	$ php artisan migrate


# Session 04 : How to Create Controllers
Go to your termial in current project and type command:
	​$ php artisan make:controller CompanyController

	
# Session 05 : How to Create Views

	
# Session 06 : How to Configure Route and Route Resource

# Session 07 : Configure Style and Javascript
	In block of tag <head>....</head> show like below:
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="csrf-token" content="{{ csrf_token() }}">

			<title> Laravel 8 Project </title>

			<script src="js/app.js" defer></script>
			<link href="css/app.css" rel="stylesheet">    
		</head>
		
		We have change to below code:
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="csrf-token" content="{{ csrf_token() }}">

			<title> Laravel 8 Project </title>

			<script src="{{ asset('js/app.js') }}" defer></script>
			<link href="{{ asset('css/app.css') }}" rel="stylesheet">    
		</head>
		
		* NOTE : We use {{asset('put path of file') }}

# Session 08 : Create Master layout
	Go to Resources/views/layouts and create file:
		- master.blade.php
		- folder: includes , and in folder includes have to create files:
			- styles.blade.php
			- scrypes.blade.php
			- sidebar.blade.php
			- nav.blade.php
		

# Session 09 : Create List of user
	Go to Resources/views/admin/users : create file name:
		- index.blade.php
		- list.blade.php
		


